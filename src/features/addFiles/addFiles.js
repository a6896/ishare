import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { 
  Breadcrumb, Button,
  Card, Col, Divider, Image, Layout, Menu, PageHeader, Row, Spin, Typography, Upload, message } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css'; 
import { fetchLinks, setUri } from './addFilesSlice';

function AddFiles({ node }) {

  const props = {
    name: 'file',
    directory: true,
    progress: (prog) => {
      console.log("PROGRESS >>>> ", prog)
    }
  }
  const onChange = async (e) => {
    console.log("ON CHANGE ", e)
    const allFiles = e.target.files.map(f => {
      console.log("FILE > ", f)
      return {
        path: f.webkitdirectory,
        content: f
      }
    })
    console.log("RESULT > ", allFiles)

    // const globSourceOptions = {
    //   recursive: true
    // }

    const addOptions = {
      pin: true,
      wrapWithDirectory: true,
      timeout: 10000
    }

    for await (const result of node.addAll(allFiles.map(f => { return { path: f.originFileObj.webkitRelativePath, content: f } }), addOptions)) {
      console.log("RSEULT >>>> ", result)
    }
  }
  async function selectFolder(e) {
    const addOptions = {
      pin: true,
      wrapWithDirectory: true,
      timeout: 10000
    }
    console.log("FILES ", Array.from(e.target.files))
    const files = Array.from(e.target.files).map(f => { return { path: '/images/' + f.name, content: f } });
    console.log("E > ", e)
    // for (var i = 0; i < e.target.files.length; i++) {
    //    var s = e.target.files[i].name + '\n';
    //    s += e.target.files[i].size + ' Bytes\n';
    //    s += e.target.files[i].type;
    //    alert(s);
    // }
    for await (const result of node.addAll(files, addOptions)) {

      console.log("RSEULT >>>> ", result)
    }
    
 }
  return (
    <>
      <Layout.Content>
          <Row justify="center" >
            {/* <input type="file" onChange={onChange} webkitdirectory mozdirectory msdirectory odirectory directory multiple="multiple" /> */}
            <input type="file" onChange={selectFolder} webkitdirectory directory multiple />

            {/* <input multiple directory webkitdirectory mozdirectory type="file" onChange={onChange}/> */}
            {/* <Upload {...props}>
              <p className="ant-upload-drag-icon">
                <InboxOutlined />
              </p>y
              <p className="ant-upload-text">Click or drag file to this area to upload</p>
              <p className="ant-upload-hint">
                Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                band files
              </p>
            </Upload> */}
            {/* <Button onClick={() => onChange() }>Upload</Button> */}
          </Row>
      </Layout.Content>
    </>
  );
}

export default AddFiles;