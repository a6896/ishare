import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { create } from "ipfs-http-client";
import { CID } from 'multiformats/cid'
import * as json from 'multiformats/codecs/json'
import { sha256 } from 'multiformats/hashes/sha2'
import { base64 } from "multiformats/bases/base64"

export const fetchLinks = createAsyncThunk('ipfs/fetchLinks', async (node, uri) => {
    const getLinkProperties = ({ name, path, size, type, cid }) => { return { name, path, size, type, cid: cid.toString() } }
    const links = [];
    console.log("FETCH LINKS > ", node, uri)
    for await (const link of node.ls(uri)) {
        links.push(getLinkProperties(link));
    }
    const isDir = links[0].type === 'dir';

    return { links, isDir, uri };
});

export const fileManagerSlice = createSlice({
    name: 'fileManager',
    initialState: {
        // root: 'http://localhost:5001/api/v0',
        loading: false,
        mangas: [],
        pages: [],
        uri: "/ipfs/QmdKuqmAw5P1r8Ds4ZW8mn8prUuqefNqbLfe9FoGarUizj",
        // url: 'http://bafybeig6v3ktqktlwioqx6jacpunrlp5fqydp4fvpmzh5jn2rbbusengqa.ipfs.localhost:48080'
    },
    reducers: {
        addLinks: (state, { isDir, links }) => {
            if (isDir) {
                state.mangas =links;
            } else {
                state.pages = links;
            }
        },

        // setLinks: (state, { type, payload }) => {
        //     const links = payload;
        //     console.log("SET LINKS > ", payload, links)
        //     const isDir = links[0].type === 'dir';
        //     if (isDir) {
        //         state.mangas = links;
        //     } else {
        //         state.pages = links;
        //     }
        //     return state;
        // },

        // fetchLinks: async (state, { type, payload }) => {
            
        //     // const version = await node.version();
        //     const links = [];
        //     for await (const link of node.ls(payload)) {
        //         links.push(link);

        //         console.log(link)
        //     }

        //     console.log("STATE ", state, state.mangas, state.pages)
        // },


        // setNode: async (state) => {
        //     state.node = create({ url: 'http://localhost:5001/api/v0' })
        // },
        setUri: (state, { payload }) => {
            state.uri = payload;
        }
    },
    extraReducers(builder) {
        builder
            .addCase(fetchLinks.pending, (state) => {
                state.loading = true;
            })
            .addCase(fetchLinks.fulfilled, (state, { payload }) => {
                state.loading = false;
                const { isDir, links } = payload;
                if (isDir) {
                    state.mangas = links;
                    state.pages = [];
                } else {
                    state.mangas = [];
                    state.pages = links;
                }
            })
            .addCase(fetchLinks.rejected, (state, { payload }) => {
                state.loading = false;
                state.mangas = [];
                state.pages = [];
            })
    }
})

export const { addLinks, setLinks, setUri, showPages } = fileManagerSlice.actions

export default fileManagerSlice.reducer