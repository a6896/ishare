import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { 
  Breadcrumb, Button,
  Card, Col, Image, Input, Layout, Menu, PageHeader, Row, Spin, Switch, Typography } from 'antd';
import { FolderFilled, FileAddOutlined, HomeOutlined, ReadFilled, ShareAltOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css'; 
import { fetchLinks, setApi, setUri } from './fileManagerSlice';
// import urlSource from 'ipfs-utils/src/files/url-source';

const { Paragraph, Text } = Typography;

function FileManager({ node }) {
  // const node = useSelector((state) => state.fileManager.node)
  const { loading, mangas, pages, api, uri } = useSelector((state) => state.fileManager)

  const dispatch = useDispatch()

  // const linksFunc = async() => {
  //   const result = await dispatch(fetchLinks(uri)).unwrap();
  //   console.log("RESULT > ", result)
  //   return result.links
  // }

  // useEffect(async () => {
  //   let pages = await linksFunc()
  //   console.log("LINKS FUNC ", linksFunc())
  // }, [dispatch])

  useEffect(() => {
    console.log("USE EFFECT >>>> ", api, uri)
    // const links = await fetchLinks("/ipfs/QmdKuqmAw5P1r8Ds4ZW8mn8prUuqefNqbLfe9FoGarUizj")
    dispatch(fetchLinks(node, api, uri));
    // await fetchLinks(uri)
  }, [api, uri])


  // zoomPage(index => {
  //   console.log("ZOOM FROM PAGE ", index)
  // })

  // readFromPage(index => {
  //   console.log("READ FROM PAGE")
  // })

  const spinnerContainer = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    'MozTransform': 'translateX(-50%) translateY(-50%)',
    'WebkitTransform': 'translateX(-50%) translateY(-50%)',
    transform: 'translateX(-50%) translateY(-50%)'
  }

  // const routes = uri.split('/').map(link => { 
  //   console.log("ROUTES > ", link)
  //   return { path: link, breadcrumbName: link }} )

  // const currentDir = routes[routes.length - 1].name;

  const separator = <span style={{ color: 'white'}}>/</span>

  const collapsed = true;

  const onChangeApi = (toggled) => {
    if (toggled) {
      dispatch(setApi('pin'));
    } else {
      dispatch(setApi('files'));
    }
  }

  const onChangeUri = (e) => {
    dispatch(setUri(e.target.value));
  }

  return (
    <>
      <Row gutter={[16, 16]}>
        <Col span={4}>
          <Switch checkedChildren='files' unCheckedChildren='pin' defaultChecked onChange={onChangeApi}/>
        </Col>
        <Col span={20}>
          <Input placeholder='CID' onChange={onChangeUri}/>
        </Col>
        <div style={spinnerContainer}>
          <Spin size='large' spinning={loading} />
        </div>
      
        { mangas !== undefined && pages.length === 0 && mangas.map(page => (
        <Col key={page.path} span={6} xs={24} sm={12} md={6} lg={6}>
          <Card onClick={() => dispatch(setUri(page.path))} title={page.name} actions={[
            <Button type='primary' shape='circle' icon={<ShareAltOutlined />} onClick={() => console.log("SHARE ", page.path) }></Button>,
          ]}>
            <FolderFilled style={{ fontSize: '60px', horizontalAlign: 'middle'}}/>
            <Paragraph>
            <a>
            {page.cid.toString()}</a>
            </Paragraph>
          </Card>
        </Col>
        ))}

        { pages !== undefined && pages.map((page, index) => (
          <Col key={page.path} span={6} xs={24} sm={12} md={12} lg={6}>
          <Card style={{width: '100%', height: '100%'}} onClick={() => console.log("zoomPage", index)}>
            <Image
              width={'100%'}
              src={'/' + page.path}
            />
            {/* <Text code>
              {page.path}
            </Text> */}
            <Link to="/reader">
              <Button type="primary" shape="circle" size='large' icon={<ReadFilled style={{ fontSize: '20px'}}/>} onClick={() => console.log("readFromPage", index)}/>
            </Link>
          </Card>
        </Col>
        ))}
      </Row>
  </>
  );
}

export default FileManager;