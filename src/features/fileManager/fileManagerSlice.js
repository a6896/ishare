import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { CID } from 'multiformats/cid'
import * as json from 'multiformats/codecs/json'
import { sha256 } from 'multiformats/hashes/sha2'
import { base64 } from "multiformats/bases/base64"


export const fetchLinks = createAsyncThunk('ipfs/fetchLinks', async (node, api, uri) => {
    const getLinkProperties = ({ name, path, size, type, cid }) => { return { name, path, size, type, cid: cid.toString() } }
    const links = [];
    console.log("FETCH LINKS > ", api, uri)
    for await (const link of node[api].ls(uri)) {
        console.log('link', link)
        links.push(getLinkProperties(link));
    }
    const isDir = links[0].type === 'dir';

    return { links, isDir, uri };
});

export const fileManagerSlice = createSlice({
    name: 'fileManager',
    initialState: {
        loading: false,
        mangas: [],
        pages: [],
        api: 'pin',
        uri: "/"
    },
    reducers: {
        addLinks: (state, { isDir, links }) => {
            if (isDir) {
                state.mangas = links;
            } else {
                state.pages = links;
            }
        },
        setUri: (state, { payload }) => {
            state.uri = payload;
        },
        setApi: (state, { payload }) => {
            state.api = payload;
        }
    },
    extraReducers(builder) {
        builder
            .addCase(fetchLinks.pending, (state) => {
                state.loading = true;
            })
            .addCase(fetchLinks.fulfilled, (state, { payload }) => {
                state.loading = false;
                const { isDir, links } = payload;
                if (isDir) {
                    state.mangas = links;
                    state.pages = [];
                } else {
                    state.mangas = [];
                    state.pages = links;
                }
            })
            .addCase(fetchLinks.rejected, (state, { payload }) => {
                state.loading = false;
                state.mangas = [];
                state.pages = [];
            })
    }
})

export const { addLinks, setApi, setLinks, setUri } = fileManagerSlice.actions

export default fileManagerSlice.reducer