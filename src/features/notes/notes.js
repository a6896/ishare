import React, { useEffect } from 'react';
import { Outlet, Routes, Route, } from 'react-router-dom';
import Overview from './overview.js';
import Edit from './edit.js';
import Editor from './editor.js';
import all from 'it-all';
import toBuffer from 'it-to-buffer';

function Notes({ node }) {
    const [notes, setNotes] = React.useState([])
    const [note, setNote] = React.useState({ title: '', content: '' })


    useEffect(async () => {
        console.log("OVER VIEW > ", node)
        // const response1 = await node.files.mkdir('/notes')
        let allNotes = [];
        try {
            allNotes = await all(node.files.ls('/notes'));
        } catch(err) {
            if (err.code === 'ERR_NOT_FOUND') {
                console.log("ERR NOT FOUND >")
                await node.files.mkdir('/notes');
                allNotes = await all(node.files.ls('/notes'));
            }
            console.log("err ", err.code === 'ERR_NOT_FOUND', Object.keys(err))
        }
        const textDecoder = new TextDecoder();
        for (var i = 0; i < allNotes.length; i++) {
            const content = await toBuffer(node.files.read('/notes/' + allNotes[i].name))
            console.log("CONTENT > ", textDecoder.decode(content))
            
            allNotes[i].content = textDecoder.decode(content);
        }
        setNotes(allNotes)

    }, [])


    return (
    <>
        <Routes>
            <Route path="/" element={<Overview node={node} notes={notes} />} />
            <Route path="create" element={<Editor node={node} />} />
            <Route path=":index" element={<Edit node={node} notes={notes} />} />
        </Routes>
    </>
    );
}

export default Notes;