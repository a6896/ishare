import React, { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import { 
   Button, Divider, Form, Input, Layout } from 'antd';
import { SaveOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css'; 
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.bubble.css';


function Editor({ node, notes }) {
    const navigate = useNavigate();
    const params = useParams();
    console.log("PROPS ",  node, notes)
    const [note, saveNote ] = React.useState({})
    const [title, setTitle] = React.useState('')
    const onSave = async () => {
        const formattedTitle = title.replace(/ /g, '_');
        const result = await node.files.write('/notes/' + title + '.html', note, { create: true })
        navigate('/notes/list');
    }

    useEffect(() => {
        console.log("PROPS > USE EFFECT ", notes)
        // saveNote({text: note.text })
        // setTitle(title)
    }, [])

    const modules = {
        toolbar: [
          [{ 'header': [1, 2, false] }],
          ['bold', 'italic', 'underline','strike', 'blockquote'],
          [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
          ['link', 'image'],
          ['clean']
        ],
      }
    
    //   formats: [
    //     'header',
    //     'bold', 'italic', 'underline', 'strike', 'blockquote',
    //     'list', 'bullet', 'indent',
    //     'link', 'image'
    //   ]
    // }
    return (
    <>
        <Layout.Content>
            <Form>
                <Form.Item name='title' label='Title' rules={[{ required: true, message: 'Please enter a descriptive note title' }]}>
                    <Input
                        placeholder='Title'
                        bordered={true}
                        value={title}
                        onChange={e => setTitle(e.target.value)}/>
                </Form.Item>
                <Divider type='horizontal'/>
                <ReactQuill theme="bubble" modules={modules} key={title} onChange={saveNote}/>
                <Form.Item>
                    <Button type='primary' htmlType='submit' icon={<SaveOutlined />} onClick={onSave}>Save</Button>
                </Form.Item>
            </Form>

        </Layout.Content>
    </>
    );
}

Editor.propTypes = {
    title: PropTypes.string,
    content: PropTypes.string
}
export default Editor;