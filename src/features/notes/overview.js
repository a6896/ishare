import React, { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { 
   Button, Card, Col, Divider, Input, Layout, Row, Typography } from 'antd';
import { DeleteOutlined, EditOutlined, FolderFilled, FileAddOutlined, HomeOutlined, PlusOutlined, ReadFilled, ShareAltOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css'; 
import Editor from './editor.js';
// import Drafts from './drafts.js';
import all from 'it-all';
import toBuffer from 'it-to-buffer';

const { Paragraph, Text } = Typography;

function Overview({ node, notes }) {

    const remove = async note => {
        console.log("REMOVE NOTE ", note)
        const index = notes.findIndex(n => n.name === note.name);
        console.log(notes.filter(n => n.cid.toString() !== note.cid.toString()));
        const content = await toBuffer(node.files.rm('/notes/' + notes[index].name))
    }

    const renderHtmlString = str => (
        <div dangerouslySetInnerHTML={{ __html: str }} />
    );

    return (
    <>
        <Layout.Content>
            <Row>
                <Divider orientation='left'><Link to="/notes/create"><Button type='primary' shape='circle' icon={<PlusOutlined />}/></Link></Divider>
            </Row>
            <Row gutter={16}>
            {notes !== undefined && notes.map((note, index) => (
                <Col key={index} span={24} xs={24} sm={12} md={12} lg={6}>
                    <Card style={{width: '100%', height: '100%'}} title={note.name.split('.')[0]} actions={[
                        <Link to={`/notes/${index}`} ><Button type='primary' shape='circle' icon={<EditOutlined />}></Button></Link>,
                        <Button shape='circle' icon={<DeleteOutlined />}  onClick={() => remove(note) }></Button>
                        ]}>
                        {renderHtmlString(note.content.substring(0, 50) + '...')}
                    </Card>
                </Col>
            ))}
            </Row>
        </Layout.Content>
    </>
    );
}

export default Overview;