import React, { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import { 
   Button, Divider, Form, Input, Layout } from 'antd';
import { SaveOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css'; 
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.bubble.css';
import Editor from './editor.js';


function Edit({ node, notes }) {
    const navigate = useNavigate();
    const params = useParams();
    console.log("PARAMS ", params)
    console.log("PROPS ",  node, notes[parseInt(params.index)])
    const [note, saveNote ] = React.useState(notes[parseInt(params.index)].content)
    const [title, setTitle] = React.useState('')
    const defaultTitle = notes[parseInt(params.index)].name.split('.')[0];
    console.log("SET TITLE ", title)
    const onSave = async () => {
        const formattedTitle = title.replace(/ /g, '_');
        const result = await node.files.write('/notes/' + title + '.html', note, { create: true })
        navigate('/notes');
    }

    const modules = {
        toolbar: [
          [{ 'header': [1, 2, false] }],
          ['bold', 'italic', 'underline','strike', 'blockquote'],
          [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
          ['link', 'image'],
          ['clean'],
          [{'color': []}, {'background': []}]
        ],
      }

      const formats = [
        'header',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image', 'color', 'background'
      ];
    return (
    <>
        <Layout.Content>
            <Form>
                <Form.Item name='title' label='Title' rules={[{ required: true, message: 'Please enter a descriptive note title' }]}>
                    <Input
                        placeholder='Title'
                        bordered={true}
                        defaultValue={defaultTitle}
                        value={title}
                        onChange={e => setTitle(e.target.value)}/>
                </Form.Item>
                <Divider type='horizontal'/>
                <ReactQuill theme="bubble" modules={modules} formats={formats} key={title}/>
                <Form.Item>
                    <Button type='primary' htmlType='submit' icon={<SaveOutlined />} onClick={onSave}>Save</Button>
                </Form.Item>
            </Form>

        </Layout.Content>
    </>
    );
}

Edit.propTypes = {
    title: PropTypes.string,
    content: PropTypes.string
}
export default Edit;