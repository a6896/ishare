import React from 'react';
// import React, { useEffect } from 'react';
// import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
// import { 
//   // Breadcrumb, 
//   Card, Col, Image, Row, Typography } from 'antd';
import { FolderFilled, ReadFilled } from '@ant-design/icons';
import 'antd/dist/antd.css';
// import { Carousel, CarouselItem } from 'react-onsenui';
import { Card, Row, Col, Image } from 'antd';

const MangaReader = () => {
    const { mangas, pages, uri } = useSelector((state) => state.fileManager)
    const [isFullScreen, setIsFullScreen] = React.useState(false)

    const dispatch = useDispatch()
    let image;
    if (isFullScreen) {
        image = <Image.PreviewGroup>
            { pages.map((page, index) => (
                <Image key="index" src={'http://localhost:8080' + page.path} />
            ))}
        </Image.PreviewGroup>
    } else {
        image = <Row style={{ width: '100vw', height: '100vh'}} gutter={[16, 16]}>
        { pages.map((page, index) => (
            <Col key={page.path} span={8}>
                <Card>
                    <Image
                        preview={false}
                        width={'100%'}
                        src={'http://localhost:8080' + page.path}
                        onClick={setIsFullScreen}
                    />
                </Card>
            </Col>
            ))}
        </Row>
    }
    const fullWidth = {
        width: '100vh'
    }
    return (
        <Image.PreviewGroup style={fullWidth}>
        { pages.map((page, index) => (
            <Image key="page.path" src={'http://localhost:8080' + page.path} preview={false} style={fullWidth} />
        ))}
    </Image.PreviewGroup>
    );
}

export default MangaReader;
