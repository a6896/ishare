import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { 
  Breadcrumb, Button,
  Card, Col, Image, Layout, Menu, PageHeader, Row, Spin, Typography } from 'antd';
import { FolderFilled, FileAddOutlined, HomeOutlined, ReadFilled, ShareAltOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css'; 
import { fetchLinks, setUri } from './shareFileSlice';
// import urlSource from 'ipfs-utils/src/files/url-source';

const { Paragraph, Text } = Typography;

function ShareFiles() {
  // const node = useSelector((state) => state.fileManager.node)
  const { loading, mangas, pages, uri } = useSelector((state) => state.fileManager)

  const dispatch = useDispatch()

  // const linksFunc = async() => {
  //   const result = await dispatch(fetchLinks(uri)).unwrap();
  //   console.log("RESULT > ", result)
  //   return result.links
  // }

  // useEffect(async () => {
  //   let pages = await linksFunc()
  //   console.log("LINKS FUNC ", linksFunc())
  // }, [dispatch])

  useEffect(() => {
    console.log("USE EFFECT >>>> ", uri)
    // const links = await fetchLinks("/ipfs/QmdKuqmAw5P1r8Ds4ZW8mn8prUuqefNqbLfe9FoGarUizj")
    dispatch(fetchLinks(uri));
    // await fetchLinks(uri)
  }, [uri])


  // zoomPage(index => {
  //   console.log("ZOOM FROM PAGE ", index)
  // })

  // readFromPage(index => {
  //   console.log("READ FROM PAGE")
  // })

  const spinnerContainer = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    'MozTransform': 'translateX(-50%) translateY(-50%)',
    'WebkitTransform': 'translateX(-50%) translateY(-50%)',
    transform: 'translateX(-50%) translateY(-50%)'
  }

  const routes = uri.split('/').map(link => { 
    console.log("ROUTES > ", link)
    return { path: link, breadcrumbName: link }} )

  const currentDir = routes[routes.length - 1].name;

  const separator = <span style={{ color: 'white'}}>/</span>

  const collapsed = true;

  return (
    <>
    <Layout style={{minHeight: '100vh'}}>
      {/* <PageHeader
        title={currentDir}
        breadcrumb={{routes}}
        ghost={false}
        onClick={(param) => console.log(param)}
      /> */}
      {/* <Layout.Sider collapsible collapsed={collapsed}>
        <div>
        <Menu theme="dark" defaultSelectedKeys={['1']} mode='inline' onClick={(item) => console.log("ITEM ", item)}>
          <Menu.Item icon={<HomeOutlined />} key="home">Home</Menu.Item>
          <Menu.Item icon={<FileAddOutlined />} key="add">Add</Menu.Item>
          <Menu.Item icon={<ShareAltOutlined />} key="share">Share</Menu.Item>
        </Menu>
        </div>
      </Layout.Sider> */}
      {/* <Layout className="site-layout"> */}
      <Layout.Header>
      <Breadcrumb separator={separator}>
      {uri.split('/').map(path => 
        <Breadcrumb.Item style={{ color: 'white' }} key={path}><a href={uri} style={{ color: 'white'}} onClick={(event) => {
          event.preventDefault();
          const splitUri = uri.split('/');
          const index = splitUri.indexOf(path);
          const newUri = splitUri.slice(0, index + 1).join('/');
          dispatch(setUri(newUri));
        }}>{path}</a></Breadcrumb.Item>
      )}
      </Breadcrumb>
      </Layout.Header>
      <Layout.Content className="site-layout-background" style={{ marginTop: '10px', marginLeft: '10px', marginRight: '10px'}}>
      <div>
      <Row gutter={[16, 16]}>

      <div style={spinnerContainer}>
        <Spin size='large' spinning={loading} />
      </div>
      
      { mangas !== undefined && pages.length === 0 && mangas.map(page => (
        <Col key={page.path} span={6}>
          <Card onClick={() => dispatch(setUri(page.path))}>
            <FolderFilled style={{ fontSize: '60px', horizontalAlign: 'middle'}}/>
            <Paragraph>
            {page.name} / <a>
            {page.cid.toString()}</a>
            </Paragraph>
        </Card>
        </Col>
        ))}

        { pages !== undefined && pages.map((page, index) => (
          <Col key={page.path} span={6}>
            <Card style={{width: '100%', height: '100%'}} onClick={() => console.log("zoomPage", index)}>
              <Image
                width={'100%'}
                src={'http://localhost:8080' + page.path}
              />
              {/* <Text code>
                {page.path}
              </Text> */}
              <Link to="/reader">
                <Button type="primary" shape="circle" size='large' icon={<ReadFilled style={{ fontSize: '20px'}}/>} onClick={() => console.log("readFromPage", index)}/>
              </Link>
            </Card>
          </Col>
        ))}
      </Row>
      </div>
      </Layout.Content>
      </Layout>
      {/* </Layout> */}
    </>
  );
}

export default ShareFiles;