import * as IPFS from 'ipfs-core';

// const runIpfs = async () => {
//     const node = await IPFS.create();
//     console.log("NODE CREATED")
//     const stream = node.cat('QmfNkhFkCKuYotnEznYsZDKKDcA4Ch28ZbN9hMmDo8MqEz')
//     let data = ''
    
//     for await (const chunk of stream) {
//       // chunks of data are returned as a Buffer, convert it back to a string
//       data += chunk.toString()
//     }
    
//     console.log(data)
//     }
//     runIpfs();
export function register() {
    
}

export function unregister() {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.ready.then(registration => {
            registration.unregister();
        })
    }
}