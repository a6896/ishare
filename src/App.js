import React from 'react';
import { Route, Routes } from 'react-router-dom';
import AppLayout from './app/appLayout.js';
import MangaReader from './features/mangaReader/mangaReader.js';

class App extends React.Component {
  render() {
    return (
      <>
        <AppLayout />
      </>
    );
  }
}

export default App;
