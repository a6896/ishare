import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link, Outlet, Route, Routes } from 'react-router-dom';
import AddFiles from '../features/addFiles/addFiles.js';
import FileManager from '../features/fileManager/fileManager.js';
import MangaReader from '../features/mangaReader/mangaReader.js';
import ShareFiles from '../features/shareFiles/shareFiles.js';
import Notes from '../features/notes/notes.js';
import { 
  Breadcrumb, Button,
  Card, Col, Image, Layout, Menu, PageHeader, Row, Spin, Typography } from 'antd';
import { EditOutlined, FileAddOutlined, HomeOutlined, ShareAltOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css'; 
import { createNode } from './appSlice.js';

const { Paragraph, Text } = Typography;

function AppLayout() {
  const [isFullScreen, setIsFullScreen] = React.useState(false)
  const [currentComponent, setCurrentComponent] = React.useState('home')
  const [selectedKeys, onSelect] = React.useState(['home'])
  const [collapsed, setCollapsed] = React.useState(true);
  const { node, loading } = useSelector((state) => state.app)
  const dispatch = useDispatch()

  useEffect(async () => {
    dispatch(createNode())
// const identity = await node.id()
//     console.log("IDENTITY > ", identity)
// const config = node.config.getAll();
// console.log("CONFIG ", config)
// const keyList = node.key.list()
// console.log("KEY LIST > ", keyList)
  }, [])

  const spinnerContainer = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    'MozTransform': 'translateX(-50%) translateY(-50%)',
    'WebkitTransform': 'translateX(-50%) translateY(-50%)',
    transform: 'translateX(-50%) translateY(-50%)'
  }

  const separator = <span style={{ color: 'white'}}>/</span>

  const menuItems = {
    home: <FileManager />,
    add: <AddFiles />,
    share: <ShareFiles />
  }

  // const onSelect = ({ item, key, keyPath, selectedKeys, domEvent }) => {
  //   console.log("ON OPEN CHANGE >>> ", item, key, keyPath, selectedKeys)

  //     setSelectedKeys(item);

  // };

  const hasNode = () => node !== undefined

  return (
    <>
      <Layout style={{minHeight: '100vh'}}>
        {/* <PageHeader
          title={currentDir}
          breadcrumb={{routes}}
          ghost={false}
          onClick={(param) => console.log(param)}
        /> */}
        <Layout.Sider collapsible collapsed={collapsed} onCollapse={() => setCollapsed(!collapsed)}>
            <Menu 
              theme="dark"
              defaultSelectedKeys={selectedKeys}
              mode='inline'
              selectedKeys={selectedKeys}
              onSelect={onSelect}
              inlineCollapsed={true}>
             <Menu.Item icon={<Link to='/'><HomeOutlined /></Link>} key="home">Home</Menu.Item>
              <Menu.Item icon={<Link to='/add'><FileAddOutlined /></Link>} key="add">Add</Menu.Item>
              <Menu.Item icon={<Link to='/share'><ShareAltOutlined /></Link>} key="share">Share</Menu.Item>
              <Menu.Item icon={<Link to='/notes'><EditOutlined /></Link>} key="notes">Notes</Menu.Item>
            </Menu>
        </Layout.Sider>
        <Layout className="site-layout">
          {/* <Layout.Header>
            <Breadcrumb separator={separator}>
            {uri.split('/').map(path => 
              <Breadcrumb.Item style={{ color: 'white' }} key={path}><a href={uri} style={{ color: 'white'}} onClick={(event) => {
                event.preventDefault();
                const splitUri = uri.split('/');
                const index = splitUri.indexOf(path);
                const newUri = splitUri.slice(0, index + 1).join('/');
                dispatch(setUri(newUri));
              }}>{path}</a></Breadcrumb.Item>
            )}
            </Breadcrumb>
          </Layout.Header> */}
        <Layout.Content className="site-layout-background" style={{ marginTop: '10px', marginLeft: '10px', marginRight: '10px'}}>

          {loading && <div style={spinnerContainer}>
            <Spin size='large' spinning={loading} />
          </div>}

          {!loading && node && <Routes>
            <Route path="/" element={<FileManager node={node} />} />
            <Route path="/share" element={<ShareFiles node={node} />} />
            <Route path="/add" element={<AddFiles node={node} />} />
            <Route path="/reader" element={<MangaReader node={node} />} />
            <Route path="/notes/*" element={<Notes node={node}/>} />
          </Routes>}
          <Outlet />
          
          {/* {menuItems[currentComponent]} */}
        </Layout.Content>
      </Layout>
    </Layout>
  </>
  );
}

export default AppLayout;