import { configureStore } from '@reduxjs/toolkit'
import fileManagerReducer from '../features/fileManager/fileManagerSlice'
import appReducer from './appSlice'


export default configureStore({
    reducer: {
        app: appReducer,
        fileManager: fileManagerReducer
    }
})