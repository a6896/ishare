import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { CID } from 'multiformats/cid'
import * as json from 'multiformats/codecs/json'
import { sha256 } from 'multiformats/hashes/sha2'
import { base64 } from "multiformats/bases/base64"
import * as IPFS from 'ipfs';

export const createNode = createAsyncThunk('ipfs/createNode', async () => {
    const node = await IPFS.create();
    console.log("NEW NODE >> ", node)
    return node;
});

export const appSlice = createSlice({
    name: 'app',
    initialState: {
        loading: false,
        node: undefined
    },
    reducers: {
    },
    extraReducers(builder) {
        builder
            .addCase(createNode.pending, (state) => {
                state.loading = true;
            })
            .addCase(createNode.fulfilled, (state, { payload }) => {
                state.node = payload;
                state.loading = false;
                console.log("PAYLOAD >> APP ", payload)
            })
            .addCase(createNode.rejected, (state, {}) => {
                state.node = undefined;
                state.loading = false;
            })
    }
})

export const { } = appSlice.actions

export default appSlice.reducer